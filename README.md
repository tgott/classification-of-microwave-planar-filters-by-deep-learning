# BUT Dataset: Classification of Microwave Planar Filters by Deep Learning
Over the last few decades, deep learning has been considered to be powerful tool in the classification tasks, and has become popular in many applications due to its capability of processing huge amount of data. This paper presents approaches for image recognition. We have applied convolutional neural networks on microwave planar filters. The first task was filter topology classification, the second task was filter order estimation. For the task a dataset was generated. As presented in the results, the created and trained neural networks are very capable of solving the selected tasks.

# Citation
@article{vesely2021NN,
  title={Classification of Microwave Planar Filters by Deep Learning},
  author={Vesely, J., Olivova, J., Gotthans, J., Gotthans, T., Raida, Z.},
  journal={Radioengineering},
  volume={},
  pages={},
  year={},
}

